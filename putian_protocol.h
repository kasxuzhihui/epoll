#ifndef __PUTIAN_PROTOCOL_H
#define __PUTIAN_PROTOCOL_H


#include "hash.h"
#include "mqtt_control.h"
#include "Cbuffer.h"
#include "cJSON.h"
#include "tcp_server.h"






extern unsigned char fd_uid_table[MAX_DEVICE_NUM][6];
extern int fds[100];//临时保存 mqtt接收平台信息的fds[]数组
extern unsigned char uids[100][6];//临时保存 mqtt接收平台信息的uids[]数组
extern unsigned char temp_uid[13];//临时保存 mqtt接收平台信息的uids[]数组中的单个uid字符串

void *tcp_send_pthread(void *data);
void rc_use_tcp_init(void);
void *tcp_recv_porcess_pthread(void *data);


extern void putian_control_light(int num,unsigned char lightHead,unsigned char numOfLightHead,int dimmer);//控制
extern void putian_time_query(int fd);
extern void putian_task_set(int num,int task_num,unsigned char time,unsigned char *buf,unsigned char lightHead);
extern void putian_longitude_set(int num,unsigned char *lon,unsigned char *lat);

extern void StrToHex(unsigned char *pbDest, unsigned char *pbSrc, int nLen);


#endif

