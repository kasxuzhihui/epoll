#include "tcp_server.h"


void setFdNonblock(int fd)
{
    fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) | O_NONBLOCK);
}

void err_exit(const char *s){
    printf("error: %s\n",s);
    exit(0);
}

int create_socket(const char *ip, const int port_number)
{
    struct sockaddr_in server_addr = {0};
    server_addr.sin_family = AF_INET;           /* ipv4 */
    server_addr.sin_port = htons(port_number);
    if(inet_pton(server_addr.sin_family, ip, &server_addr.sin_addr) == -1){
        err_exit("inet_pton");
    }
    int sockfd = socket(PF_INET, SOCK_STREAM, 0);
    if(sockfd == -1){
        err_exit("socket");
    }
    int reuse = 1;
    if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1)
    {
        err_exit("setsockopt");
    }
    if(bind(sockfd, (sockaddr *)&server_addr, sizeof(server_addr)) == -1){
        err_exit("bind");
    }
    if(listen(sockfd, 5) == -1){
        err_exit("listen");
    }
    return sockfd;
}




const int MAX_EPOLL_EVENTS = MAX_DEVICE_NUM;

int epollfd;
void *epoll_server_start(void *data)
{
    const char * ip = "localhost";
    const int port = PUTIAN_SERVER_POART;
    int sockfd = create_socket(ip, port);
    setFdNonblock(sockfd);
    epollfd = epoll_create1(0);
    if(epollfd == -1) err_exit("epoll_create1");
    struct epoll_event ev;
    ev.data.fd = sockfd;
    ev.events = EPOLLIN ;
    if(epoll_ctl(epollfd, EPOLL_CTL_ADD, sockfd, &ev) == -1){
        err_exit("epoll_ctl1");
    }
    struct epoll_event events[MAX_EPOLL_EVENTS] = {0};

    while(1)
	{
        int number = epoll_wait(epollfd, events, MAX_EPOLL_EVENTS, -1);     
        if(number > 0){
            for (int i = 0; i < number; i++)
            {
                int eventfd = events[i].data.fd;
                if(eventfd == sockfd)
				{
					#ifdef LOG4C
                    log4c(LOG_PATH,"accept new client start\r\n");
                    #endif
                    struct sockaddr_in client_addr;
                    socklen_t client_addr_len = sizeof(client_addr);
                    int connfd = accept(sockfd, (struct sockaddr *)&client_addr, &client_addr_len);
                    setFdNonblock(connfd);
                    if(connfd<0)
                    {
                       //printf("connfd<0 \n");
                       log4c(LOG_PATH,"connfd<0 that is accept 5012 is error. !\r\n");
                    }
                    struct epoll_event ev;
                    ev.data.fd = connfd;
                    ev.events = EPOLLIN;
                    if(epoll_ctl(epollfd, EPOLL_CTL_ADD, connfd, &ev) == -1){
                        err_exit("epoll_ctl2");
                    }
					
                    //printf("accept new client end  fd=%d. \n",connfd);
                    #ifdef LOG4C
                    log4c(LOG_PATH,"accept new client end  fd=%d !\r\n",connfd);
                    #endif
					
                }
                else
				{
                    while(1)
					{
                        
						struct RC_TCP_RECV_DATA recv_data={0};
						
                        int ret = read(eventfd, recv_data.recv_buf, 300);
						
                        if(ret > 0)
						{
                            if(( recv_data.recv_buf[6]==0x1a)&&( recv_data.recv_buf[7]==0x01))
                            {
                                if(( recv_data.recv_buf[12]==0xff)&&( recv_data.recv_buf[13]==0xff)&&( recv_data.recv_buf[14]==0xff)&&( recv_data.recv_buf[15]==0xff))//只有此项目需要此步骤
                                {
                                    // for(int k=0;k<ret;k++)
                                    // {
                                    //     printf("%02X ", recv_data.recv_buf[k]);
                                    // }
                                    //printf("error recv end  fd =%d uid=%d %d %d %d\n ",eventfd,recv_data.recv_buf[12],recv_data.recv_buf[13],recv_data.recv_buf[14],recv_data.recv_buf[15]); 
                                    //#ifdef LOG4C
                                    log4c(LOG_PATH,"error recv buffer  fd =%d uid=%02X%02X%02X%02X%02X%02X !\r\n",eventfd,recv_data.recv_buf[10],recv_data.recv_buf[11],recv_data.recv_buf[12],
                                    recv_data.recv_buf[13],recv_data.recv_buf[14],recv_data.recv_buf[15]);
                                    //#endif
                                    epoll_ctl(epollfd, EPOLL_CTL_DEL, eventfd, NULL);
                                    close(eventfd);
                                    break;
                                }else
                                {
                                    //printf("accept recv OK  fd =%d uid=%02X %02X %02X %02X\n ",eventfd,recv_data.recv_buf[12],recv_data.recv_buf[13],recv_data.recv_buf[14],recv_data.recv_buf[15]); 
                                    #ifdef LOG4C
                                    log4c(LOG_PATH,"accept recv OK  fd =%d uid=%02X%02X%02X%02X%02X%02X !\r\n",eventfd,recv_data.recv_buf[10],recv_data.recv_buf[11],recv_data.recv_buf[12],
                                    recv_data.recv_buf[13],recv_data.recv_buf[14],recv_data.recv_buf[15]);
                                    #endif
                                }

                            }
                            recv_data.recv_size=ret;
							recv_data.fd=eventfd;
                            rc_tcp_recv_data_enqueue(&tcp_recv_cmd,&recv_data);

                            #ifdef LOG
							for(int k=0;k<ret;k++)
	                        {
	                        	printf("%02X ", recv_data.recv_buf[k]);
							}
							printf("putian recv end  \r\n "); 
							#endif
                            
	                        break;
                        }else if (ret == 0)
                        {
                           // printf("client close %d.\n",eventfd);
                            log4c(LOG_PATH,"client close %d. !\r\n",eventfd);
                            epoll_ctl(epollfd, EPOLL_CTL_DEL, eventfd, NULL);
                            close(eventfd);
                            
                            break;
                        }else if(ret < 0)
                        {
                            //printf("read error %d.\n",eventfd);
                            log4c(LOG_PATH,"read error %d. !\r\n",eventfd);
                            epoll_ctl(epollfd, EPOLL_CTL_DEL, eventfd, NULL);
			                close(eventfd);
                            
                            break;
                        }
                    }
                    
                }
            }
        }
    }
    log4c(LOG_PATH,"while 1 is out\r\n");
	return NULL;
}

void close_epoll_fd(int fd)
{
    epoll_ctl(epollfd, EPOLL_CTL_DEL, fd, NULL);
	close(fd);

}



