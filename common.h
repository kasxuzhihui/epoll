#ifndef __COMMON_H
#define __COMMON_H

#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <pthread.h>
#include <string>
#include <iostream> 
#include<cstring>
#include <pthread.h> 
#include <time.h> 
#include<ctype.h>
#include <stdarg.h>

// #define LOG
// #define HASH_LOG
// #define LOG_onoffline_test
// #define MQTT_LOG
// #define TCP_LOG
// #define UIDS_LOG
// #define LOG4C

#define MQTT_SERVER_IP "49.234.32.251"
#define MQTT_SERVER_POART 1883
#define PUTIAN_SERVER_POART 5012
#define UUID 				"24eda3ae"
#define VERSION 			"mcu20"
#define NAP 				"Rich.Lamp"
#define INIT 				"Init"
#define CONTROL 			"Control"
#define PUSH 				"Push"

#define ERROR_INFO "sudo systemctl restart putian-server.service"
#define LOG_PATH "/root/projects/epoll/log.txt"
#define MAX_DEVICE_NUM 3000

/**MQTT need modify***/

//if connect the same YUNserver length<15
#define MQTT_ClientIdentifier "xutest123" 


/**MQTT end**/

extern void	hc_dsptrn( const uint8_t *target_ptr ,uint8_t *start_ptr,uint16_t size8);
extern int log4c(char *filepath, char *fmt, ...);

#endif

