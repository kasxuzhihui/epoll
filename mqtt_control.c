#include "mqtt_control.h"


Mqtt_State_Type mqtt_state;
int cfd_mqtt;
struct sockaddr_in s_add_cfd_mqtt;


unsigned char socket_connect_to_mqtt_init()
{	
	//char *name="device.iot.sihenggroup.com";
	char ipstr[16];
	//struct hostent *answer;
	//answer = gethostbyname(name);
	//if(answer==NULL)
	if(0)
	{
		#ifdef LOG 
		printf("******dns fail !******\r\n");
		#endif
		
		return 0;
	}
	//inet_ntop(AF_INET, answer->h_addr_list[0], ipstr, 16);
	cfd_mqtt = socket(AF_INET, SOCK_STREAM, 0);
	bzero(&s_add_cfd_mqtt,sizeof(struct sockaddr_in));
	s_add_cfd_mqtt.sin_family=AF_INET;
	//s_add_cfd_siheng.sin_addr.s_addr= inet_addr(ipstr);
	s_add_cfd_mqtt.sin_addr.s_addr= inet_addr(MQTT_SERVER_IP);
	s_add_cfd_mqtt.sin_port=htons(MQTT_SERVER_POART);
	if(-1 == connect(cfd_mqtt,(struct sockaddr *)(&s_add_cfd_mqtt), sizeof(struct sockaddr)))
	{
		#ifdef LOG 
		printf("connect fail !\r\n");
		#endif
		
		return 0;
			
	}
	
	#ifdef LOG 
	printf("connect %s ok !\r\n",ipstr);
	#endif
	
	return 1;
	
}





void mqtt_connet_req(void)
{
	struct RC_MQTT_SEND_DATA mqtt_data={0};
	
	unsigned short int num;
	num = GetDataConnet(mqtt_data.snd_buf);

	mqtt_data.snd_size=num;
	#ifdef MQTT_LOG
	for(int i=0;i<num;i++)
	{
		printf("%02x.",mqtt_data.snd_buf[i]);
	}
	printf("----MQTT enqueue  mqtt_connet_req INFO\r\n");
	#endif
	rc_mqtt_send_data_enqueue(&mqtt_send_cmd,&mqtt_data);
	
}

char sub_topic[60]="$s2d/24eda3ae/mcu20/20200223121100/send88888";

//$s2d/{{uuid}}/{{gatewayVersion}}/{{gatewayId}}/send
void mqtt_sub_req(void)
{
	struct RC_MQTT_SEND_DATA mqtt_data={0};
	
	unsigned short int num;
	sprintf(sub_topic,"$s2s/%s/PTlamp/send",UUID);
	num=GetDataSUBSCRIBE(mqtt_data.snd_buf,(const char*)sub_topic,1,0);
	
	mqtt_data.snd_size=num;
	#ifdef MQTT_LOG
	for(int i=0;i<num;i++)
	{
		printf("%02x.",mqtt_data.snd_buf[i]);
	}
	printf("----MQTT enqueue  mqtt_sub_req INFO\r\n");
	#endif
	rc_mqtt_send_data_enqueue(&mqtt_send_cmd,&mqtt_data);
}


void mqtt_ping_req(void)
{
	struct RC_MQTT_SEND_DATA mqtt_data={0};
	GetDataPINGREQ(mqtt_data.snd_buf);
	mqtt_data.snd_size=2;
	rc_mqtt_send_data_enqueue(&mqtt_send_cmd,&mqtt_data);
}



//$d2s/{{uuid}}/{{gatewayVersion}}/push


void mqtt_publish_req(unsigned char *snd_buf,Message_Publish_Topic topic)
{
	struct RC_MQTT_SEND_DATA mqtt_data={0};
	unsigned short int snd_num;
	char pub_topic[60];
	switch(topic)
	{
		case Topic_Pub_PTlamp_PUSH:
			sprintf((char *)pub_topic,"$s2s/%s/PTlamp/push",UUID);
			break;
		case Topic_Pub_PTlamp2_PUSH:
			sprintf((char *)pub_topic,"$s2s/%s/PTlamp2/push",UUID);
			break;
		default:
			break;
	}
	snd_num=GetDataPUBLISH(mqtt_data.snd_buf,0,0,0,(const char*)pub_topic,(const char*)snd_buf);
	mqtt_data.snd_size=snd_num;
	rc_mqtt_send_data_enqueue(&mqtt_send_cmd,&mqtt_data);
}






void	mqtt_recv_process(unsigned char *rcv_buf,unsigned short int rcv_num)
{
 unsigned char code = *rcv_buf;
 code=code>>4;
 switch(code)
 {
	case MQTT_TypeCONNACK:
		break;
	case MQTT_TypeSUBACK:
		break;
	default:
		break;
 }



}
Mqtt_State_Type mqtt_mqtt_state_inquire(void)
{
	return mqtt_state;
}

void mqtt_set_mqtt_state(Mqtt_State_Type state)
{
	mqtt_state = state;
}


/*******************************
Mqtt_State_connect_req---Mqtt_State_connect_req_wait---recv Mqtt_State_sub_req---
Mqtt_State_sub_req----Mqtt_State_sub_req_wait----recv Mqtt_State_Retain_Bug_wait
Mqtt_State_connect_ok

*********************************/
unsigned char mqtt_connect_open_Per2s(void)
{
	static unsigned char t=0;
	switch(mqtt_state)
	{
		case Mqtt_State_connect_req:
			mqtt_set_mqtt_state(Mqtt_State_connect_req_wait);
			mqtt_connet_req();
			
			break;
		case Mqtt_State_sub_req:
			mqtt_set_mqtt_state(Mqtt_State_sub_req_wait);
			mqtt_sub_req();
			
			break;
		case Mqtt_State_Retain_Bug_wait:
 			mqtt_set_mqtt_state(Mqtt_State_connect_ok);
 		case Mqtt_State_connect_ok:
			return 1;
		default:
			break;
	}
	t++;
	if(t>30)
	{
		t=0;
		return 2;
	}
	return 0;
}






unsigned char mqtt_pingack_num;

void MQTT_Connect_Check_Per2S(void)
{	
	mqtt_pingack_num++;
	#ifdef LOG
	//printf("mqtt_pingack_num =%d \n",mqtt_pingack_num);	
	#endif
	if(mqtt_pingack_num>=45)
	{
	  #ifdef LOG
	  printf("mqtt reset");
	  #endif
	  exit(0);
	}
}

				


TcpReceived_Data tcpreceived_data;
TcpRevBuff tcp_revbuf;

void rczn_js_protocol_process(void)
{

	cJSON* read_json=NULL;
	cJSON* read_json1=NULL;
	cJSON* read_json2=NULL;
	//cJSON* read_json3=NULL;
	cJSON* read_jsonArray_fd=NULL;
	cJSON* read_jsonArray_uid=NULL;
	cJSON* read_jsonArray_task=NULL;
	cJSON* read_jsonArray_time=NULL;

    int protocolType;
	int num=0,task_num=0;
	int taskTime[8];
	unsigned char sendTaskTime=0;
	unsigned char taskBuf[37];
	
	
	read_json=cJSON_Parse((const char*)tcpreceived_data.data);
	sscanf(cJSON_GetObjectItem(read_json,"protocolType")->valuestring,"%4X",&protocolType);
	read_jsonArray_fd=cJSON_GetObjectItem(read_json,"fds");
	read_jsonArray_uid=cJSON_GetObjectItem(read_json,"uids");
	num=cJSON_GetArraySize(read_jsonArray_fd);

	#ifdef LOG
	printf("nu1  ==%d ,protocolType=%d \r\n",num,protocolType);
	#endif

	read_json1=cJSON_GetObjectItem(read_json,"data");
	
	
	if(num>22)//最多同时发21个设备，超过了不行
	{
		return;
	}
	
	for(int m=0;m<num;m++)
	{
		fds[m]=cJSON_GetArrayItem(read_jsonArray_fd,m)->valueint;
		//printf("fds[%d]=%d\r\n",m,fds[m]);
	}
	for(int mm=0;mm<num;mm++)
	{
		hc_dsptrn((uint8_t *)cJSON_GetArrayItem(read_jsonArray_uid,mm)->valuestring,(uint8_t *)&temp_uid[0],12);
		temp_uid[12]='\0';
		StrToHex(&uids[mm][0],&temp_uid[0],6);
		#ifdef UIDS_LOG
		printf("UID uids==%02X%02X%02X%02X%02X%02X \r\n",uids[mm][0],uids[mm][1],uids[mm][2],uids[mm][3],uids[mm][4],uids[mm][5]);
		#endif
	}
	#ifdef LOG
	printf("mqtt   fdNum==%d ,data =%s\r\n",num,tcpreceived_data.data);
	#endif
	
	switch(protocolType)
	{
		case 0x2A01://浜や簰妯″紡璁剧疆
			
			break;
		case 0x2A02://涓嬪彂鎺у埗
			putian_control_light(num,1,1,cJSON_GetObjectItem(read_json1,"dimmer")->valueint);
			break;
		case 0x2A09://璁惧鏃堕棿鏌ヨ
			putian_time_query(fds[0]);
			break;
		case 0x2A05://浠诲姟璁剧疆
			read_jsonArray_time = cJSON_GetObjectItem(read_json,"timeType");
			for(int m=0;m<8;m++)
			{
				taskTime[m]=cJSON_GetArrayItem(read_jsonArray_time,m)->valueint;
				#ifdef LOG
				printf("sendTaskTime[%d]=%02x\r\n",m,sendTaskTime);
				
				#endif
				sendTaskTime|=((taskTime[m]&0x01)<<(7-m));
			}
			read_jsonArray_task = cJSON_GetObjectItem(read_json1,"taskInfo");
			task_num=cJSON_GetArraySize(read_jsonArray_task);
			if((task_num>=1)&&(task_num<=6))
			{
				for(int i=0;i<task_num;i++)
				{
					
					read_json2=cJSON_GetArrayItem(read_jsonArray_task,i);
						
					
					taskBuf[i*6]=cJSON_GetObjectItem(read_json2,"mode")->valueint;
					taskBuf[1+i*6]=cJSON_GetObjectItem(read_json2,"hour")->valueint;
					taskBuf[2+i*6]=cJSON_GetObjectItem(read_json2,"min")->valueint;
					taskBuf[3+i*6]=0;
					taskBuf[4+i*6]=cJSON_GetObjectItem(read_json2,"onoff")->valueint;
					taskBuf[5+i*6]=cJSON_GetObjectItem(read_json2,"dimmer")->valueint;
					#ifdef LOG
					printf("MQTT.C  task buf%d,%d,%d,%d,%d,%d \r\n",
					taskBuf[i*6],taskBuf[1+i*6],taskBuf[2+i*6],taskBuf[3+i*6],taskBuf[4+i*6],taskBuf[5+i*6]);
					#endif
				}
				putian_task_set(num,task_num,sendTaskTime,(unsigned char *)taskBuf,1);
			}else
			{
				printf("task num error \r\n");
			}
			break;
		case 0x2A0A:
			//putian_longitude_set(num,sendTaskTime,(unsigned char *)taskTime);
			break;
			
		default:
			break;
	}
	
		
	
}


void TCP_rczn_recv_analysis_1(void)
{	
	unsigned char code;
	unsigned short i,j=0,len=0,start_n;
	unsigned short size = 0;//size  js锟秸碉拷锟杰筹拷锟斤拷
	
	if(tcp_revbuf.read_index!=tcp_revbuf.write_index)
	{
		code = tcp_revbuf.tcp_rev_buf[tcp_revbuf.read_index]>>4;
		
		switch(code)
		{
			case MQTT_TypeCONNACK:
				mqtt_set_mqtt_state(Mqtt_State_sub_req);
				len = 2+tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+1)&0x3ff];
				break;
			case MQTT_TypePUBLISH:
				if(mqtt_mqtt_state_inquire() == Mqtt_State_Retain_Bug_wait)
				{
					tcp_revbuf.read_index=0;
					tcp_revbuf.write_index=0;
					memset(tcp_revbuf.tcp_rev_buf,0,1024);
					return;
				}
				if(tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+1)&0x3ff]>=128)
				{
					len = 3+(tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+1)&0x3ff]-128)+
						(tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+2)&0x3ff]*128);
					
					size = len-3-2-tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+4)&0x3ff];
					start_n = 3+2+tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+4)&0x3ff];
				}else
				{
					len = 2+tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+1)&0x3ff];
					size = len-2-2-tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+3)&0x3ff];
					start_n = 2+2+tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+3)&0x3ff];
				}
				break;
			case MQTT_TypeSUBACK:
				mqtt_set_mqtt_state(Mqtt_State_Retain_Bug_wait);
				len = 2+tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+1)&0x3ff];
				break;
			case MQTT_TypePINGRESP:
				mqtt_pingack_num=0;
				len = 2+tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+1)&0x3ff];
				break;
			default:
				tcp_revbuf.read_index=0;
				tcp_revbuf.write_index=0;
				memset(tcp_revbuf.tcp_rev_buf,0,1024);
				return;
		}
		if((size<=TCP_RECV_DATA_MAX)&&(size!=0))
		{
			for(i=0;i<size;i++)
			{
				
				tcpreceived_data.data[i]=tcp_revbuf.tcp_rev_buf[(tcp_revbuf.read_index+i+start_n)&0x3ff];
			}
			tcpreceived_data.data[i-j+1]='\0';
			rczn_js_protocol_process();
			
			memset(tcpreceived_data.data,0,i);
		}
		
		tcp_revbuf.read_index=((tcp_revbuf.read_index+len)&0x3ff);
	}
}


void recv_process()
{
	unsigned char socket_rev_buffer[1024]={0};
	unsigned short recbyte;
	
	recbyte = recv(cfd_mqtt, socket_rev_buffer, 1024,0);
	if(recbyte<=0)
	{
		printf("----MQTT client error\r\n");
		exit(0);
	}else
	{

	}
	for(int i=0;i<recbyte;i++)
	{
		#ifdef MQTT_LOG
		printf("%02x,",socket_rev_buffer[i]);
		#endif
		tcp_revbuf.tcp_rev_buf[((tcp_revbuf.write_index+i)&0x3ff)]=socket_rev_buffer[i];
	}
	#ifdef MQTT_LOG
	printf("----MQTT recv INFO\r\n");
	#endif

	tcp_revbuf.write_index=((tcp_revbuf.write_index+recbyte)&0x3ff);
	
	TCP_rczn_recv_analysis_1();	
}


void *mqtt_recv_pthread(void *data)
{
	
	while(1)
	{
		
		recv_process();
						
	}
    return NULL;


}




void mqtt_send_data_func(void)
{
	struct RC_MQTT_SEND_DATA	*mqtt_send_data = NULL;
	
	mqtt_send_data = rc_mqtt_send_data_dequeue(&mqtt_send_cmd);
	
	#ifdef MQTT_LOG
	for(int i=0;i<mqtt_send_data->snd_size;i++)
	{
		printf("%02x.",mqtt_send_data->snd_buf[i]);
	}
	printf("----MQTT dequeue send INFO\r\n");
	#endif


	send(cfd_mqtt, mqtt_send_data->snd_buf, mqtt_send_data->snd_size, 0);
	free(mqtt_send_data);


}


void *mqtt_send_pthread(void *data)
{

	while(1)
	{
		
		mqtt_send_data_func();
		usleep(5);		
					
		
		
	}
    return NULL;

	

}

void rc_use_mqtt_init(void)
{
	mqtt_set_mqtt_state(Mqtt_State_connect_req);
	//cbuf_init(tcp_send_cmd);

	

	

}

unsigned char MQTT_start_task(void)
{
	static unsigned char system_status=0;
	
	pthread_t mqtt_recv_pthread_tid,mqtt_send_pthread_tid;
	
	switch(system_status)
	{
		case 0:
			if(socket_connect_to_mqtt_init())
			{
				system_status =1;
				pthread_create(&mqtt_recv_pthread_tid,NULL,mqtt_recv_pthread,0);
				pthread_create(&mqtt_send_pthread_tid,NULL,mqtt_send_pthread,0);
                mqtt_set_mqtt_state(Mqtt_State_connect_req);
				
			}else
			{
				printf("mqtt port:1883 connetct error! \r\n");
			}
            usleep(100);
			break;
		case 1:
			switch(mqtt_connect_open_Per2s())
			{
				case 0:
					break;
				case 1:
					system_status =2;
					break;
				case 2:
					//锟斤拷锟斤拷
					printf("mqtt connect timeout \r\n");
					break;
				default:
					break;
					
			}
			sleep(2);
			break;
		case 2:
			#ifdef MQTT_LOG
			printf("start mqtt and serve ok\n");
			#endif
			sleep(1);
			
			break;
	}
	return system_status;
}
