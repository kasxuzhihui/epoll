#include "mqtt_control.h"


unsigned char send_i=0;

union UDATA
{
    uint16_t B;      //����������
    uint8_t C[2];
};

//------------------------------------------------------------------------------
//CRC��λ�ֽ�ֵ��
//------------------------------------------------------------------------------
unsigned char CRCHi[256] =
{
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
  0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40
};
//------------------------------------------------------------------------
//CRC��λ�ֽ�ֵ��
//------------------------------------------------------------------------
unsigned char CRCLo[256] =
{
  0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7,
  0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,
  0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9,
  0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
  0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
  0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,
  0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D,
  0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
  0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF,
  0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
  0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1,
  0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
  0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB,
  0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
  0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
  0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
  0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97,
  0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,
  0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89,
  0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
  0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83,
  0x41, 0x81, 0x80, 0x40
};


//-------------------------------------------------------------------------
//��������:CRC16
//
//
//����˵����
//-------------------------------------------------------------------------
unsigned int CRC16(unsigned char *pushMsg,unsigned char usDataLen)
{
	unsigned char uchCRCHi=0xff;
	unsigned char uchCRCLo=0xff;
	unsigned char uchIndex;
	while(usDataLen--)
	{
		uchIndex=uchCRCHi^*pushMsg++;
		uchCRCHi=uchCRCLo^CRCHi[uchIndex];
		uchCRCLo=CRCLo[uchIndex];
	}
	return((unsigned int)uchCRCHi<<8|uchCRCLo);
	
}










typedef union  
{
    unsigned char  uc[4];   
    float          f;
}Float4Byte;



float byte_convert_float(unsigned char *buf)
{
	Float4Byte u;
	u.uc[0]=buf[3];
	u.uc[1]=buf[2];
	u.uc[2]=buf[1];
	u.uc[3]=buf[0];
	return u.f;

}

unsigned char js_buf[940];

unsigned char fd_uid_table[MAX_DEVICE_NUM][6];
int fds[100];//临时保存 mqtt接收平台信息的fds[]数组
unsigned char uids[100][6];//临时保存 mqtt接收平台信息的uids[]数组
unsigned char temp_uid[13];//临时保存 mqtt接收平台信息的uids[]数组中的单个uid字符串

void putian_recv_process(unsigned char *buf,unsigned char size,int fd)
{
	time_t now;
	struct tm *timenow;
	
	unsigned char snd_buf[300];
	
	union UDATA crc_value;
	
	struct RC_TCP_SEND_DATA tcp_data={0};
	//st_putianInfo redis_set={0};

	

	
	unsigned short int start_code = ((buf[0]<<8)+buf[1]);
	unsigned short int protocol_version = ((buf[2]<<8)+buf[3]);
	//unsigned short int length = ((buf[4]<<8)+buf[5]);
	unsigned short int protocol_type = ((buf[6]<<8)+buf[7]);
	unsigned short int info_serial_num = ((buf[8]<<8)+buf[9]);
	
	
	if((fd<0)||(fd>=(MAX_DEVICE_NUM-200)))
	{
		
		log4c(LOG_PATH,"fd=%d is overange \r\n",fd);
		system(ERROR_INFO);
		return;
	}
	
	crc_value.B=CRC16(&buf[6],size-10);
	
	if((crc_value.C[0]==buf[size-4])&&(crc_value.C[1]==buf[size-3]))//crc ok
	{
		#ifdef LOG
		printf("crcok \r\n");
		#endif
	}
	if((start_code==0xB1A5)&&(protocol_version==0xA001))
	{
	}else
	{
	  //#ifdef LOG4C
	  log4c(LOG_PATH,"protocol_version or start_code is error!fd=%d  buf[0-1]=%02X%02X buf[12-14]=%02X%02X%02X\r\n",fd,buf[0],buf[1],buf[12],buf[13],buf[14]);
	  //#endif
	  close_epoll_fd(fd);
	  return;
	}
	switch(protocol_type)
	{
		case 0x1A01://login
			//save uid and fd
			//redis_set.fd=fd;
			//hc_dsptrn(&buf[10],&redis_set.uid[0],6);
			//rc_set_putianInfo_by_key(&fd,4,&redis_set);
			hc_dsptrn(&buf[10],&fd_uid_table[fd][0],6);
			//#ifdef LOG4C
			log4c(LOG_PATH,"putian light login %02X%02X%02X%02X%02X%02X!  and save fd=%d \r\n",buf[10],buf[11],buf[12],buf[13],buf[14],buf[15],fd);
			//#endif
			//end save uid and fd
			
				sprintf((char *)js_buf, "{\"protocolType\":\"%04X\",\"uid\":\"%02X%02X%02X%02X%02X%02X\",\"fd\":%d,\
			\"data\":{}}",
			protocol_type,buf[10],buf[11],buf[12],buf[13],buf[14],buf[15],fd);
			mqtt_publish_req(js_buf,Topic_Pub_PTlamp_PUSH);
			
			time(&now);
			timenow = localtime(&now);
			
			

			
			snd_buf[0]= 0xb1;
			snd_buf[1]= 0xa5;
			
			snd_buf[2]= 0Xa0;
			snd_buf[3]= 0x01;
			/*�����͵�CRC16�ĳ���*/
			snd_buf[4]= 0x00;
			snd_buf[5]= 0x28;
			
			snd_buf[6]= 0x1b;
			snd_buf[7]= 0x01;
			
			snd_buf[8]= ((info_serial_num>>8)&0xff);
			snd_buf[9]= (info_serial_num&0xff);
			
			snd_buf[10]= buf[10];
			snd_buf[11]= buf[11];
			snd_buf[12]= buf[12];
			snd_buf[13]= buf[13];
			snd_buf[14]= buf[14];
			snd_buf[15]= buf[15];
			
			snd_buf[16]= 0x80;
			snd_buf[17]= 0xf0;
			snd_buf[18]= 0x01;
			
			snd_buf[19]= timenow->tm_year-100; /* 年份，其值从1900开始 */
			snd_buf[20]= timenow->tm_mon+1;/* [0,11] */
			snd_buf[21]= timenow->tm_mday;/* [1,31] */
	        if(timenow->tm_wday==0)
	        {
	            snd_buf[22]=7;
	        }
	        else
	        {
	            snd_buf[22]= timenow->tm_wday;/* [0,6] */
	        }
			snd_buf[23]= timenow->tm_hour;/* [0,23] */
			snd_buf[24]= timenow->tm_min;/* [0,59] */
			snd_buf[25]= timenow->tm_sec;/* [0,59] */ 
			
			snd_buf[26]= 0;
			snd_buf[27]= 0;
			snd_buf[28]= 0;
			snd_buf[29]= 0;
			snd_buf[30]= 0;
			snd_buf[31]= 0;
			snd_buf[32]= 0;
			snd_buf[33]= 0;
			
			snd_buf[34]= 0;
			snd_buf[35]= 0;
			snd_buf[36]= 0;
			snd_buf[37]= 0;
			snd_buf[38]= 0;
			snd_buf[39]= 0;
			snd_buf[40]= 0;
			snd_buf[41]= 0;
			
			/*2G��
			0x01ÿ10���������ϱ�״̬��Ĭ�ϣ�
			0x02ÿ30���������ϱ�״̬
			0x03ÿ�������ϱ�״̬
			NB��
			0x04 ÿ10�����ϱ�һ��
			0x05ÿһСʱ�ϱ�һ��
			0x06 �����ϱ�*/
			snd_buf[42]= 1;
			/*0-������ 1-�ɵ���*/
			snd_buf[43]= 1;
			
			/*ʹ��CRC16-ModBus�����͵�CRC16֮ǰУ��*/

			crc_value.B=CRC16(&snd_buf[6],38);
			snd_buf[44]= crc_value.C[0];
			snd_buf[45]= crc_value.C[1];
			
			snd_buf[46]= 0x1b;
			snd_buf[47]= 0x5a;

			
			tcp_data.fd=fd;
			tcp_data.snd_size=48;
			memcpy(tcp_data.snd_buf,snd_buf,48);
			rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);
			
			break;
		
		case 0x1A02://heartbeat

		
		#ifdef LOG_onoffline_test
		printf("heartbeat \r\n");
		#endif
		snd_buf[0]= 0xb1; //起始符
		snd_buf[1]= 0xa5;
		
		snd_buf[2]= 0Xa0;//协议版本号
		snd_buf[3]= 0x01;
		
		snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
		snd_buf[5]= 0x0f;
		
		snd_buf[6]= 0x1b;
		snd_buf[7]= 0x02;//类型，CRC16从此处开始校验
		
		snd_buf[8]= ((info_serial_num>>8)&0xff);
		snd_buf[9]= (info_serial_num&0xff);//信息序列号
		
		snd_buf[10]= buf[10];
		snd_buf[11]= buf[11];
		snd_buf[12]= buf[12];
		snd_buf[13]= buf[13];
		snd_buf[14]= buf[14];
		snd_buf[15]= buf[15];//uid
		
		


		snd_buf[16]= 0x80;
		snd_buf[17]= 0xf1;
		snd_buf[18]= 0x01;
		
		/*使用CRC16-modbus对类型到CRC16之前校验*/

		crc_value.B=CRC16(&snd_buf[6],13);
		snd_buf[19]= crc_value.C[0];
		snd_buf[20]= crc_value.C[1];
		
		snd_buf[21]= 0x1b;
		snd_buf[22]= 0x5a;
		
		
		tcp_data.fd=fd;
		tcp_data.snd_size=23;
		memcpy(tcp_data.snd_buf,snd_buf,23);
		rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);

		time(&now);
		timenow = localtime(&now);
		if((timenow->tm_hour == 15)&&(timenow->tm_min > 18)&&(timenow->tm_min < 33))
		{
			send_i++;	
			snd_buf[0]= 0xb1; //起始符
			snd_buf[1]= 0xa5;
			
			snd_buf[2]= 0Xa0;//协议版本号
			snd_buf[3]= 0x01;
			
			snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
			snd_buf[5]= 0x14;
			
			snd_buf[6]= 0x2a;
			snd_buf[7]= 0x08;//类型，CRC16从此处开始校验
			
			snd_buf[8]= 0;
			snd_buf[9]= send_i;//信息序列号
			
			snd_buf[10]= buf[10];
			snd_buf[11]= buf[11];
			snd_buf[12]= buf[12];
			snd_buf[13]= buf[13];
			snd_buf[14]= buf[14];
			snd_buf[15]= buf[15];//uid
			
			


			snd_buf[16]= 0x8c;
			snd_buf[17]= timenow->tm_year-100; /* 年份，其值从1900开始 */
			snd_buf[18]= timenow->tm_mon+1;/* [0,11] */
			snd_buf[19]= timenow->tm_mday;/* [1,31] */
			if(timenow->tm_wday==0)
			{
				snd_buf[20]=7;
			}
			else
			{
				snd_buf[20]= timenow->tm_wday;/* [0,6] */
			}
			snd_buf[21]= timenow->tm_hour;/* [0,23] */
			snd_buf[22]= timenow->tm_min;/* [0,59] */
			snd_buf[23]= timenow->tm_sec;/* [0,59] */ 
			
			/*使用CRC16-modbus对类型到CRC16之前校验*/

			crc_value.B=CRC16(&snd_buf[6],18);
			snd_buf[24]= crc_value.C[0];
			snd_buf[25]= crc_value.C[1];
			
			snd_buf[26]= 0x1b;
			snd_buf[27]= 0x5a;
			
			
			tcp_data.fd=fd;
			tcp_data.snd_size=28;
			memcpy(tcp_data.snd_buf,snd_buf,28);
			rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);

		}
		break;

		
		case 0x1A03://report
		
		//buf[16]  				命令字			
		//buf[17]-buf[20]  		工作时长 		float
		//buf[21]-buf[24]  	 	倾斜				float
		//buf[25]-buf[40]  	 	reserve  			float
		//buf[41]   			灯头数量
		//buf[42]   			灯杆故障数量
		//buf[43]-buf[46]   	灯杆故障码		float			0xA0000001过压0xA0000008漏电
		//buf[47]   			灯头号
		//buf[48]   			灯具状态			0x01开灯 0x02关灯 0x03灯不存在
		//buf[49]   			调光率 				0x80-0xe4为精确调光 调光为0为自动关灯	
		//buf[50]-buf[53]   	U  					float
		//buf[54]-buf[57]   	I	  				float
		//buf[58]-buf[61]   	LI	  				float
		//buf[62]-buf[65]   	LU	  				float
		//buf[66]-buf[69]   	P	  				float
		//buf[70]-buf[73]   	W	  				float
		//buf[74]-buf[77]   	PF	  				float
		//buf[78]-buf[81]   	onTime  				float
		//buf[82]-buf[85]   	theoryOnTime  		float
		//buf[86]-buf[89]   	wALL  				float
		//buf[90]-buf[93]   	theorywALL  			float
		//buf[94]		   	faultLightNum  		1BYTE
		//buf[95]-buf[98]   	faultCode  			float   0xA0000002保险丝故障0xA0000003继电器故障
		//0xA0000004 补偿电容故障0xA0000005整流器故障0xA0000006触发器故障0xA0000007 灯具故障
			/*if buf[41]==2   buf[99]-buf[150]如果有灯头2*/
			
		//buf[99]-buf[106] 	报文ID
		// buf[107]-buf[108]	crc16
		// buf[109]-buf[110]	1B5A
		if(buf[48]==2){buf[48]=0;}//强行更改灯具状态 1开 0关
		if(buf[41]==1)//灯头数量为1
		{
			sprintf((char *)js_buf, "{\"protocolType\":\"%04X\",\"uid\":\"%02X%02X%02X%02X%02X%02X\",\"fd\":%d,\
		\"NumOfLightHead\":%d,\"data\":{\"tilt\":\"%10.1f\",\"onoff\":%d,\"dimmer\":%d,\"U\":\"%10.1f\",\"I\":\"%10.2f\",\
		\"LI\":\"%10.1f\",\"LU\":\"%10.1f\",\"P\":\"%10.1f\",\"W\":\"%10.2f\",\"PF\":\"%10.1f\",\"onTime\":\"%d\",\
		\"theoryOnTime\":\"%d\",\"wALL\":\"%10.2f\",\"faultLightNum\":%d,\"faultCode\":\"%02X%02X%02X%02X\"}}",
		protocol_type,buf[10],buf[11],buf[12],buf[13],buf[14],buf[15],fd,
		buf[41],byte_convert_float(&buf[21]),buf[48],(buf[49]-0x80),
		/*U I*/byte_convert_float(&buf[50]),byte_convert_float(&buf[54]),
		/*LI LU*/byte_convert_float(&buf[58]),byte_convert_float(&buf[62]),
		/*P W*/byte_convert_float(&buf[66]),(byte_convert_float(&buf[70])/3600/1000),
		/*PF onTime*/byte_convert_float(&buf[74]),((buf[78]<<24)+(buf[79]<<16)+(buf[80]<<8)+buf[81])/60,
		/*theoryOnTime wALL*/((buf[82]<<24)+(buf[83]<<16)+(buf[84]<<8)+buf[85])/60,(byte_convert_float(&buf[86])/3600/1000),
		/*faultLightNum faultCode*/buf[94],buf[95],buf[96],buf[97],buf[98]);
		mqtt_publish_req(js_buf,Topic_Pub_PTlamp_PUSH);

		}else if (buf[41]==1)
		{
			
		}else
		{
			
		}
		
		
			
		snd_buf[0]= 0xb1; //起始符
		snd_buf[1]= 0xa5;
		
		snd_buf[2]= 0Xa0;//协议版本号
		snd_buf[3]= 0x01;
		
		snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
		snd_buf[5]= 0x0d;
		
		snd_buf[6]= 0x1b;
		snd_buf[7]= 0x03;//类型，CRC16从此处开始校验
		
		snd_buf[8]= ((info_serial_num>>8)&0xff);
		snd_buf[9]= (info_serial_num&0xff);//信息序列号
		
		snd_buf[10]= buf[10];
		snd_buf[11]= buf[11];
		snd_buf[12]= buf[12];
		snd_buf[13]= buf[13];
		snd_buf[14]= buf[14];
		snd_buf[15]= buf[15];//uid
		
		snd_buf[16]= 0x46;
		
		/*使用CRC16-modbus对类型到CRC16之前校验*/

		crc_value.B=CRC16(&snd_buf[6],11);
		snd_buf[17]= crc_value.C[0];
		snd_buf[18]= crc_value.C[1];
		
		snd_buf[19]= 0x1b;
		snd_buf[20]= 0x5a;
		
		
		tcp_data.fd=fd;
		tcp_data.snd_size=21;
		memcpy(tcp_data.snd_buf,snd_buf,21);
		rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);
		break;
		
		case 0x1A04://任务执行上报
		//buf[16]  			命令字			0xb7
		//buf[17]  			执行类型		终端执行开关任务-0x01
		//buf[18]			灯头号 			0x01 灯头1  0x02 灯头2
		//buf[19]			灯具状态		0x42开灯 0x43关灯 0x47调光
		//buf[20]   		调光率			0x80-0xe4为精确调光 调光为0为自动关灯
		if(buf[19]==0x42){buf[19]=1;}
		else if(buf[19]==0x43){buf[19]=0;}
		else if(buf[19]==0x47)
		{	if((buf[20]-0x80)==0)
			{
				buf[19]=0;
			}else
			{
				buf[19]=1;
			}
		}
		else{}
		sprintf((char *)js_buf, "{\"protocolType\":\"%04X\",\"uid\":\"%02X%02X%02X%02X%02X%02X\",\"fd\":%d,\
		\"data\":{\"lightHead\":%d,\"onoff\":%d,\"dimmer\":%d}}",
		protocol_type,buf[10],buf[11],buf[12],buf[13],buf[14],buf[15],fd,
		buf[18],buf[19],(buf[20]-0x80));

		snd_buf[0]= 0xb1; //起始符
		snd_buf[1]= 0xa5;
		
		snd_buf[2]= 0Xa0;//协议版本号
		snd_buf[3]= 0x01;
		
		snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
		snd_buf[5]= 0x0d;
		
		snd_buf[6]= 0x1b;
		snd_buf[7]= 0x04;//类型，CRC16从此处开始校验
		
		snd_buf[8]= ((info_serial_num>>8)&0xff);
		snd_buf[9]= (info_serial_num&0xff);//信息序列号
		
		snd_buf[10]= buf[10];
		snd_buf[11]= buf[11];
		snd_buf[12]= buf[12];
		snd_buf[13]= buf[13];
		snd_buf[14]= buf[14];
		snd_buf[15]= buf[15];//uid
		
		snd_buf[16]= 0xb7;
		
		/*使用CRC16-modbus对类型到CRC16之前校验*/

		crc_value.B=CRC16(&snd_buf[6],11);
		snd_buf[17]= crc_value.C[0];
		snd_buf[18]= crc_value.C[1];
		
		snd_buf[19]= 0x1b;
		snd_buf[20]= 0x5a;
		
		
		tcp_data.fd=fd;
		tcp_data.snd_size=21;
		memcpy(tcp_data.snd_buf,snd_buf,21);
		rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);
		break;
		case 0x2B08://时间设置响应报文
			#ifdef MQTT_LOG
			printf("----set time result = %d\r\n",buf[19]);
			#endif
		break;
		default:
		break;
		
	}


}



void StrToHex(unsigned char *pbDest, unsigned char *pbSrc, int nLen)
{
char h1,h2;
unsigned char s1,s2;
int i;

for (i=0; i<nLen; i++)
{
h1 = pbSrc[2*i];
h2 = pbSrc[2*i+1];

s1 = toupper(h1) - 0x30;
if (s1 > 9) 
s1 -= 7;

s2 = toupper(h2) - 0x30;
if (s2 > 9) 
s2 -= 7;

pbDest[i] = s1*16 + s2;
}
}

void putian_set_comMode(unsigned char *uid,int fd)//设置交互模式
{
	union UDATA crc_value;
	unsigned char snd_buf[200];
	struct RC_TCP_SEND_DATA tcp_data;
	send_i++;
	snd_buf[0]= 0xb1; //起始符
	snd_buf[1]= 0xa5;
	
	snd_buf[2]= 0Xa0;//协议版本号
	snd_buf[3]= 0x01;
	
	snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
	snd_buf[5]= 0x0e;
	
	snd_buf[6]= 0x2a;
	snd_buf[7]= 0x01;//类型，CRC16从此处开始校验
	
	snd_buf[8]= 0;
	snd_buf[9]= send_i;//信息序列号
	
	snd_buf[10]= uid[0];
	snd_buf[11]= uid[1];
	snd_buf[12]= uid[2];
	snd_buf[13]= uid[3];
	snd_buf[14]= uid[4];
	snd_buf[15]= uid[5];//uid
	
	
	
	snd_buf[16]= 0xb3;
	snd_buf[17]= 0x01;//
	snd_buf[18]= 0x01;//调光模式 0不调光 1可调光
	
	
	crc_value.B=CRC16(&snd_buf[6],13);
	snd_buf[19]= crc_value.C[0];
	snd_buf[20]= crc_value.C[1];
	
	snd_buf[21]= 0x1b;
	snd_buf[22]= 0x5a;
	
	
	
	
	tcp_data.fd=fd;
	tcp_data.snd_size=23;
	memcpy(tcp_data.snd_buf,snd_buf,23);
	rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);

}

void putian_control_light(int num,unsigned char lightHead,unsigned char numOfLightHead,int dimmer)//控制
{
	union UDATA crc_value;
	unsigned char snd_buf[200];
	
	
	st_putianInfo *redis_get;
	struct RC_TCP_SEND_DATA tcp_data;
	
	send_i++;
	snd_buf[0]= 0xb1; //起始符
	snd_buf[1]= 0xa5;
	
	snd_buf[2]= 0Xa0;//协议版本号
	snd_buf[3]= 0x01;
	
	
	
	snd_buf[6]= 0x2a;
	snd_buf[7]= 0x02;//类型，CRC16从此处开始校验
	
	snd_buf[8]= 0;
	snd_buf[9]= send_i;//信息序列号
	
	
	
	
	
	snd_buf[16]= 0x47;
	snd_buf[17]= numOfLightHead;//灯头数量 1或2
	if (numOfLightHead==1)
	{
		
		
		printf(" 22222\r\n");
		snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
		snd_buf[5]= 0x10;
		
		snd_buf[18]= 0x01;//灯头号
		snd_buf[19]= dimmer+0x80;//调光率

		

		for(int i=0;i<num;i++)
		{
			//redis_get = rc_get_putianInfo_by_key(&fds[i],4);
			
			snd_buf[10]= fd_uid_table[fds[i]][0];
			snd_buf[11]= fd_uid_table[fds[i]][1];
			snd_buf[12]= fd_uid_table[fds[i]][2];
			snd_buf[13]= fd_uid_table[fds[i]][3];
			snd_buf[14]= fd_uid_table[fds[i]][4];
			snd_buf[15]= fd_uid_table[fds[i]][5];

			if(((snd_buf[10] != uids[i][0])||(snd_buf[11] != uids[i][1])||
				(snd_buf[12] != uids[i][2])||(snd_buf[13] != uids[i][3])||
				(snd_buf[14] != uids[i][4]))||(snd_buf[15] != uids[i][5]))
			{
				log4c(LOG_PATH,"control_light platform uid=%02X%02X%02X%02X%02X%02X and fd_uid_table is not = and fd=%d\r\n",
				uids[i][0],uids[i][1],uids[i][2],uids[i][3],uids[i][4],uids[i][5],fds[i]);
				continue;
			}
			
			

			crc_value.B=CRC16(&snd_buf[6],14);

			snd_buf[20]= crc_value.C[0];
			snd_buf[21]= crc_value.C[1];
			
			snd_buf[22]= 0x1b;
			snd_buf[23]= 0x5a;
			#ifdef log1
			for(int i=0;i<24;i++)
			{
				printf("%d,",snd_buf[i]);

			}
			printf(" send ok fds=%d\r\n" ,fds[i]);
			#endif
			
			tcp_data.fd=fds[i];
			tcp_data.snd_size=24;
			memcpy(tcp_data.snd_buf,snd_buf,24);
			rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);
			
		}
		

	}else if(numOfLightHead==2)
	{
		
		snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
		snd_buf[5]= 0x12;

		snd_buf[18]= 0x01;//灯头号
		snd_buf[19]= dimmer+0x80;//调光率
		snd_buf[20]= 0x02;//灯头号
		snd_buf[21]= dimmer+0x80;//调光率

		
		for(int i=0;i<num;i++)
		{
			//redis_get = rc_get_putianInfo_by_key(&fds[i],4);
			
			snd_buf[10]= fd_uid_table[fds[i]][0];
			snd_buf[11]= fd_uid_table[fds[i]][1];
			snd_buf[12]= fd_uid_table[fds[i]][2];
			snd_buf[13]= fd_uid_table[fds[i]][3];
			snd_buf[14]= fd_uid_table[fds[i]][4];
			snd_buf[15]= fd_uid_table[fds[i]][5];

			if(((snd_buf[10] != uids[i][0])||(snd_buf[11] != uids[i][1])||
				(snd_buf[12] != uids[i][2])||(snd_buf[13] != uids[i][3])||
				(snd_buf[14] != uids[i][4]))||(snd_buf[15] != uids[i][5]))
			{
				log4c(LOG_PATH,"control_light platform uid=%02X%02X%02X%02X%02X%02X and fd_uid_table is not = and fd=%d\r\n",
				uids[i][0],uids[i][1],uids[i][2],uids[i][3],uids[i][4],uids[i][5],fds[i]);
				
				continue;
			}
			crc_value.B=CRC16(&snd_buf[6],16);

			snd_buf[22]= crc_value.C[0];
			snd_buf[23]= crc_value.C[1];
			
			snd_buf[24]= 0x1b;
			snd_buf[25]= 0x5a;

			
			tcp_data.fd=fds[i];
			tcp_data.snd_size=26;
			memcpy(tcp_data.snd_buf,snd_buf,26);
			rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);
			
		}
		
	}else
	{
		/* code */
	}

}

void putian_time_query(int fd)
{
	struct RC_TCP_SEND_DATA tcp_data;
	st_putianInfo *redis_get;
	union UDATA crc_value;
	unsigned char snd_buf[200];
	
	send_i++;
	snd_buf[0]= 0xb1; //起始符
	snd_buf[1]= 0xa5;
	
	snd_buf[2]= 0Xa0;//协议版本号
	snd_buf[3]= 0x01;
	
	snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
	snd_buf[5]= 0x0d;
	
	snd_buf[6]= 0x2a;
	snd_buf[7]= 0x09;//类型，CRC16从此处开始校验
	
	snd_buf[8]= 0;
	snd_buf[9]= send_i;//信息序列号

	//redis_get = rc_get_putianInfo_by_key(&fd,4);
	
	snd_buf[10]= fd_uid_table[fd][0];
	snd_buf[11]= fd_uid_table[fd][1];
	snd_buf[12]= fd_uid_table[fd][2];
	snd_buf[13]= fd_uid_table[fd][3];
	snd_buf[14]= fd_uid_table[fd][4];
	snd_buf[15]= fd_uid_table[fd][5];
	
	
	
	
	snd_buf[16]= 0x8d;

	
	
	crc_value.B=CRC16(&snd_buf[6],11);
	snd_buf[17]= crc_value.C[0];
	snd_buf[18]= crc_value.C[1];
	
	snd_buf[19]= 0x1b;
	snd_buf[20]= 0x5a;
	
	
	tcp_data.fd=fd;
	tcp_data.snd_size=21;
	memcpy(tcp_data.snd_buf,snd_buf,21);
	rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);
	
}

void putian_task_set(int num,int task_num,unsigned char time,unsigned char *buf,unsigned char lightHead)
{
	
	union UDATA crc_value;
	unsigned short int length;
	st_putianInfo *redis_get;
	struct RC_TCP_SEND_DATA tcp_data;
	/*
	[task_num][1]:运行模式（执行时间类型）
					0x00-非经纬度时间
					0x01-经纬度日出前（执行时间为日出前的分钟数）
					0x02-经纬度日出后（执行时间为日出后的分钟数）
					0x03-经纬度日落前（执行时间为日落前的分钟数）
					0x04-经纬度日落后（执行时间为日落后的分钟数）
	[task_num][2]:
	[task_num][3]:0x0000（如果 RTC 模式，高字节表示小时，低字节表示分钟，即在 hh： mm）
	[task_num][4]:reserve
	[task_num][5]:开灯 0x42，关灯 0x43，调光 0x47
	[task_num][6]:调光率
	*/
	
	unsigned char snd_buf[200];
	
	send_i++;
	snd_buf[0]= 0xb1; //起始符
	snd_buf[1]= 0xa5;
	
	snd_buf[2]= 0Xa0;//协议版本号
	snd_buf[3]= 0x01;
	
	length = task_num*6+14+2;
	snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
	snd_buf[5]= length;
	
	snd_buf[6]= 0x2a;
	snd_buf[7]= 0x05;//类型，CRC16从此处开始校验
	
	snd_buf[8]= 0;
	snd_buf[9]= send_i;//信息序列号

	snd_buf[16]= 0x50;
	snd_buf[17]= lightHead;

	snd_buf[18]= time;//灯头号

	
	snd_buf[19]= task_num;//灯头号
	
	for(int i=0;i<num;i++)
	{
		//redis_get = rc_get_putianInfo_by_key(&fds[i],4);
	
		snd_buf[10]= fd_uid_table[fds[i]][0];
		snd_buf[11]= fd_uid_table[fds[i]][1];
		snd_buf[12]= fd_uid_table[fds[i]][2];
		snd_buf[13]= fd_uid_table[fds[i]][3];
		snd_buf[14]= fd_uid_table[fds[i]][4];
		snd_buf[15]= fd_uid_table[fds[i]][5];
		if(((snd_buf[10] != uids[i][0])||(snd_buf[11] != uids[i][1])||
				(snd_buf[12] != uids[i][2])||(snd_buf[13] != uids[i][3])||
				(snd_buf[14] != uids[i][4]))||(snd_buf[15] != uids[i][5]))
		{
			log4c(LOG_PATH,"putian_task_set platform uid=%02X%02X%02X%02X%02X%02X and fd_uid_table is not = and fd=%d\r\n",
				uids[i][0],uids[i][1],uids[i][2],uids[i][3],uids[i][4],uids[i][5],fds[i]);
			continue;
		}
		
		for(int j=0;j<task_num;j++)
		{
			snd_buf[20+j*6]=buf[j*6];
			snd_buf[21+j*6]=buf[1+j*6];
			snd_buf[22+j*6]=buf[2+j*6];
			snd_buf[23+j*6]=buf[3+j*6];
			snd_buf[24+j*6]=buf[4+j*6];//执行动作 0x42 开0x43关 0x47调光
			if(snd_buf[24+j*6]==0)
			{
				snd_buf[24+j*6]=0x43;
			}
			else if(snd_buf[24+j*6]==1)
			{
				snd_buf[24+j*6]=0x42;
			}else
			{

			}
			snd_buf[25+j*6]=buf[5+j*6]+0x80;
		}
		crc_value.B=CRC16(&snd_buf[6],length-2);

		snd_buf[length+4]= crc_value.C[0];
		snd_buf[length+5]= crc_value.C[1];
		
		snd_buf[length+6]= 0x1b;
		snd_buf[length+7]= 0x5a;
		
		tcp_data.fd=fds[i];
		tcp_data.snd_size=length+8;
		memcpy(tcp_data.snd_buf,snd_buf,length+8);
		rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);
	}
	
}


void putian_longitude_set(int num,unsigned char *lon,unsigned char *lat)
{
	union UDATA crc_value;
	unsigned char snd_buf[200];
	st_putianInfo *redis_get;
	struct RC_TCP_SEND_DATA tcp_data;
	send_i++;
	snd_buf[0]= 0xb1; //起始符
	snd_buf[1]= 0xa5;
	
	snd_buf[2]= 0Xa0;//协议版本号
	snd_buf[3]= 0x01;
	
	snd_buf[4]= 0x00;//长度 从类型到CRC16的长度
	snd_buf[5]= 0x1d;
	
	snd_buf[6]= 0x2a;
	snd_buf[7]= 0x0a;//类型，CRC16从此处开始校验
	
	snd_buf[8]= 0;
	snd_buf[9]= send_i;//信息序列号
	
	
	snd_buf[16]= 0xA1;
	/*longitude*/
	snd_buf[17]= lon[0];
	snd_buf[18]= lon[1];
	snd_buf[19]= lon[2];
	snd_buf[20]= lon[3];
	snd_buf[21]= lon[4];
	snd_buf[22]= lon[5];
	snd_buf[23]= lon[6];
	snd_buf[24]= lon[7];
	/*latitude*/
	snd_buf[25]= lat[0];
	snd_buf[26]= lat[1];
	snd_buf[27]= lat[2];
	snd_buf[28]= lat[3];
	snd_buf[29]= lat[4];
	snd_buf[30]= lat[5];
	snd_buf[31]= lat[6];
	snd_buf[32]= lat[7];
	
	for(int i=0;i<num;i++)
	{
		//redis_get = rc_get_putianInfo_by_key(&fds[i],4);
		
		snd_buf[10]= fd_uid_table[fds[i]][0];
		snd_buf[11]= fd_uid_table[fds[i]][1];
		snd_buf[12]= fd_uid_table[fds[i]][2];
		snd_buf[13]= fd_uid_table[fds[i]][3];
		snd_buf[14]= fd_uid_table[fds[i]][4];
		snd_buf[15]= fd_uid_table[fds[i]][5];
		if(((snd_buf[10] != uids[i][0])||(snd_buf[11] != uids[i][1])||
				(snd_buf[12] != uids[i][2])||(snd_buf[13] != uids[i][3])||
				(snd_buf[14] != uids[i][4]))||(snd_buf[15] != uids[i][5]))
		{
			log4c(LOG_PATH,"putian_longitude_set platform uid=%02X%02X%02X%02X%02X%02X and fd_uid_table is not = and fd=%d\r\n",
				uids[i][0],uids[i][1],uids[i][2],uids[i][3],uids[i][4],uids[i][5],fds[i]);
			continue;
		}
		crc_value.B=CRC16(&snd_buf[6],27);

		snd_buf[33]= crc_value.C[0];
		snd_buf[34]= crc_value.C[1];
		
		snd_buf[35]= 0x1b;
		snd_buf[36]= 0x5a;

		
		tcp_data.fd=fds[i];
		tcp_data.snd_size=37;
		memcpy(tcp_data.snd_buf,snd_buf,37);
		rc_tcp_send_data_enqueue(&tcp_send_cmd,&tcp_data);
	}
}


void tcp_recv_data_process(void)
{

	struct RC_TCP_RECV_DATA	*tcp_recv_data = NULL;
	
	tcp_recv_data = rc_tcp_recv_data_dequeue(&tcp_recv_cmd);
	
	#ifdef TCP_LOG
	for(int i=0;i<tcp_recv_data->recv_size;i++)
	{
		printf("%02x.",tcp_recv_data->recv_buf[i]);
	}
	printf("----rc_tcp_recv_data_dequeue  Info \r\n");
	#endif

	putian_recv_process(tcp_recv_data->recv_buf,tcp_recv_data->recv_size,tcp_recv_data->fd);
	
	free(tcp_recv_data);

}


void *tcp_recv_porcess_pthread(void *data)
{
	
	while(1)
	{
		
		tcp_recv_data_process();
						
	}
    return NULL;


}


void tcp_send_data_func(void)
{
	struct RC_TCP_SEND_DATA	*tcp_send_data = NULL;
	
	tcp_send_data = rc_tcp_send_data_dequeue(&tcp_send_cmd);
	
	#ifdef MQTT_LOG
	printf("---- start ues tcp send to device \r\n");
	for(int i=0;i<tcp_send_data->snd_size;i++)
	{
		printf("%02x ",tcp_send_data->snd_buf[i]);
	}
	printf("----end ues tcp send to device \r\n");
	#endif


	send(tcp_send_data->fd, tcp_send_data->snd_buf, tcp_send_data->snd_size, 0);
	
	free(tcp_send_data);


}

void *tcp_send_pthread(void *data)
{

	while(1)
	{
		
		tcp_send_data_func();
			
					
		
		
	}
    return NULL;

	

}



void rc_use_tcp_init(void)
{
	
	//cbuf_init(tcp_send_cmd);
	//cbuf_init(tcp_recv_cmd);

	

	

}

