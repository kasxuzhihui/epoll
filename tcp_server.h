#ifndef __TCP_SERVER_H
#define __TCP_SERVER_H

#include "common.h"
#include "putian_protocol.h"
#include "cJSON.h"


void *epoll_server_start(void *data);
void close_epoll_fd(int fd);
#endif

