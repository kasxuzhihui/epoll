g++ -Wall main.c cJSON.c mqtt.c common.c tcp_server.c hash.c thread.c Cbuffer.c putian_protocol.c mqtt_control.c -o serve -lpthread

cat <<EOF >/usr/lib/systemd/system/putian-server.service
[Unit]
Description=putian server service
After=rich-backend.target

[Service]
Type=simple
ExecStart=/root/projects/epoll/serve
Restart=always
RestartSec=5
LimitCORE=infinity
LimitNOFILE=10000
LimitNPROC=10000

[Install]
WantedBy=multi-user.target
EOF

systemctl enable putian-server
systemctl restart putian-server


只能放在usr/bin目录使用,参照以下命令：

g++ -Wall main.c cJSON.c mqtt.c common.c tcp_server.c hash.c thread.c Cbuffer.c putian_protocol.c mqtt_control.c -o putian-serve -lpthread

cp putian-serve /usr/bin/

cat <<EOF >/usr/lib/systemd/system/putian-server.service
[Unit]
Description=putian server service
After=rich-backend.target

[Service]
Type=simple
ExecStart=/usr/bin/putian-serve
Restart=always
RestartSec=5
LimitCORE=infinity
LimitNOFILE=10000
LimitNPROC=10000

[Install]
WantedBy=multi-user.target
EOF

systemctl enable putian-server
systemctl restart putian-server