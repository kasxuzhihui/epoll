#ifndef CBUFFER_H
#define CBUFFER_H
#include "thread.h"
#include "common.h"

//#include "HC_2_ZIGBEE_DATA.h"
#define CAPACITY 5000

typedef    struct _cbuf
{
	int        size; 		/* 当前缓冲区中存放的数据的个数 */
    int        next_in;        /* 缓冲区中下一个保存数据的位置 */
    int        next_out;        /* 从缓冲区中取出下一个数据的位置 */
    mutex_t        mutex;            /* Lock the structure */
	cond_t        not_full;        /* Full -> not full condition */
    cond_t        not_empty;        /* Empty -> not empty condition */
    void        *data[CAPACITY];/* 缓冲区中保存的数据指针 */
}cbuf_t;

extern    int        cbuf_init(cbuf_t *c);


extern    void        cbuf_destroy(cbuf_t    *c);


extern    int        cbuf_enqueue(cbuf_t *c,void *data);


extern    void*        cbuf_dequeue(cbuf_t *c);

extern int cbuf_full(cbuf_t *c);

extern int cbuf_empty(cbuf_t *c);



/*
xuzhihui modifyied
*/
struct RC_TCP_SEND_DATA
{
	int fd;
	unsigned short int snd_size;
	unsigned char snd_buf[300];
};


extern cbuf_t tcp_send_cmd;
extern struct RC_TCP_SEND_DATA* rc_tcp_send_data_dequeue(cbuf_t *c);
extern int	 rc_tcp_send_data_enqueue(cbuf_t *c,struct RC_TCP_SEND_DATA *data);





struct RC_TCP_RECV_DATA
{
	int fd;
	unsigned short int recv_size;
	unsigned char recv_buf[300];
};
extern cbuf_t tcp_recv_cmd;
extern struct RC_TCP_RECV_DATA* rc_tcp_recv_data_dequeue(cbuf_t *c);
extern int	 rc_tcp_recv_data_enqueue(cbuf_t *c,struct RC_TCP_RECV_DATA *data);






struct RC_MQTT_SEND_DATA
{
	unsigned short int snd_size;
	unsigned char snd_buf[1000];
	
};
extern cbuf_t mqtt_send_cmd;
extern struct RC_MQTT_SEND_DATA* rc_mqtt_send_data_dequeue(cbuf_t *c);
extern int	 rc_mqtt_send_data_enqueue(cbuf_t *c,struct RC_MQTT_SEND_DATA *data);



#endif

