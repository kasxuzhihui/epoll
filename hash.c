#include"hash.h"

#include<assert.h>

typedef struct hash hash_t;
typedef unsigned int (*hashfunc_t)(unsigned int, void*);

typedef struct hash_node
{
	void *key;
	void *value;
	struct hash_node *prev;
	struct hash_node *next;
}hash_node_t;


struct hash
{
	unsigned int buckets;
	hashfunc_t hash_func;
	hash_node_t **nodes;
};


hash_node_t ** hash_get_bucket(hash_t *hash,void *key);

hash_node_t * hash_get_node_by_key(hash_t *hash,void *key,unsigned int key_size);



hash_node_t ** hash_get_bucket(hash_t *hash,void *key)
{
	unsigned int bucket=hash->hash_func(hash->buckets,key);
	if(bucket>=hash->buckets)
	{
		printf("bad buckets lookup\n");
		
	}
	return &(hash->nodes[bucket]);
}

hash_node_t * hash_get_node_by_key(hash_t *hash,void *key,unsigned int key_size)
{
	hash_node_t ** bucket = hash_get_bucket(hash,key);
	hash_node_t * node=*bucket;
	if(node==NULL)
	{
		return NULL;
	}
	while(node!=NULL && memcmp(node->key,key,key_size)!=0)
	{
		node=node->next;
	}
	
	return node;
}

hash_t* hash_alloc(unsigned int buckets, hashfunc_t hash_func)
{
	hash_t *h=(hash_t *)malloc(sizeof(hash_t));
	//assert(h!=NULL);
	h->buckets=buckets;
	h->hash_func=hash_func;
	
	int size=buckets*sizeof(hash_node_t *);
	h->nodes=(hash_node_t **)malloc(size);
	memset(h->nodes,0,size);
	return h;
}

void* hash_lookup_entry(hash_t *hash, void* key, unsigned int key_size)
{
	hash_node_t *node = hash_get_node_by_key(hash, key ,key_size);
	if(node == NULL)
	{
		return NULL;
	}
	return node->value;
	
}

unsigned char hash_add_entry(hash_t *hash, void *key, unsigned int key_size,
	void *value, unsigned int value_size)
{
	//����Ѿ�����
	if(hash_get_node_by_key(hash,key,key_size))
	{
		
		return 2;
	}
	
	hash_node_t *temp=(hash_node_t *)malloc(sizeof(hash_node_t));
	temp->next=NULL;
	temp->prev=NULL;
	temp->key=malloc(key_size);
	temp->value=malloc(value_size);
	
	memcpy(temp->key,key,key_size);
	
	memcpy(temp->value,value,value_size);
	
	hash_node_t ** bucket=hash_get_bucket(hash,key);
	if(*bucket==NULL)
	{
		*bucket=temp;
		
	}
	else 
	{
		
		temp->next=*bucket;
		(*bucket)->prev=temp;
		(*bucket)=temp;
	}
	return 1;
}

void hash_free_entry(hash_t *hash, void *key, unsigned int key_size)
{
	hash_node_t * node = hash_get_node_by_key(hash,key,key_size);
	if(node==NULL)
	{
		return;
	}
	else
	{
		free(node->key);
		free(node->value);
		if(node->prev)
			node->prev->next=node->next;
		else
		{
			hash_node_t ** p = hash_get_bucket(hash,key);
			(*p)->next=node->next;
		}
		if(node->next)
			node->next->prev=node->prev;
		free(node);
	}
}













unsigned int hash_str(unsigned int buckets, void *key)
{
	char *sno=(char *)key;
	unsigned int index = 0;
	while(*sno)
	{
		index = *sno + 4*index;
		sno++;
	}
	return index % buckets;
}


unsigned int hash_int(unsigned int buckets, void *key)
{
	int  *sno=(int *)key;
	
	return (*sno) % buckets;
}

/*
void test_func(void)
{


	st_putianInfo stu2_arr[]=
	{
		{1234,"AAAA",20},
		{4568,"BBBB",20},
		{6729,"CCCC",20},
		{6731,"CCCC",31},
		{6732,"CCCC",32},
		{6733,"CCCC",33},
		{6734,"CCCC",34},
		
	};
	hash_t *hash=hash_alloc(4000000,hash_int);
	int size=sizeof(stu2_arr)/sizeof(stu2_arr[0]);
	int i;
	for(i=0;i<size;++i)
	{
		hash_add_entry(hash, &(stu2_arr[i].sno),sizeof(stu2_arr[i].sno),&stu2_arr[i],sizeof(stu2_arr[i]));
	}
	
	int sno=6734;
	st_putianInfo *stu =(st_putianInfo *)hash_lookup_entry(hash,&sno,sizeof(sno));	
	if(stu)
	{
		printf("%d %s %d\n",stu->sno,stu->name,stu->age);
	}
	else
	{
		printf("not found\n");
	}
	sno=4568;
	st_putianInfo *stu1 =(st_putianInfo *)hash_lookup_entry(hash,&sno,sizeof(sno));	
	if(stu1)
	{
		printf("%d %s %d\n",stu1->sno,stu1->name,stu1->age);
	}
	else
	{
		printf("not found\n");
	}
	sno=6733;
	st_putianInfo *stu2 =(st_putianInfo *)hash_lookup_entry(hash,&sno,sizeof(sno));	
	if(stu2)
	{
		printf("%d %s %d\n",stu2->sno,stu2->name,stu2->age);
	}
	else
	{
		printf("not found\n");
	}
	sno=6732;
	//hash_free_entry(hash,&sno,sizeof(sno));
	stu =(st_putianInfo *)hash_lookup_entry(hash,&sno,sizeof(sno));	
	if(stu)
	{
		printf("%d %s %d\n",stu->sno,stu->name,stu->age);
	}
	else
	{
		printf("not found\n");
	}
	
	
}
*/



//xuzhihui putian hash
hash_t *putian_hash;

void rc_use_hash_init(void)
{
	putian_hash=hash_alloc(1000,hash_int);//the difference is at func  hash_add_entry ,ths last if-else
	


}

st_putianInfo * rc_get_putianInfo_by_key(int *key,unsigned char key_size)
{
	st_putianInfo *stu =(st_putianInfo *)hash_lookup_entry(putian_hash,key,key_size);
	
	if(stu)
	{
		#ifdef HASH_LOG
		printf("fd = %d, uid = %02x,%02x,%02x,%02x,%02x\r\n",stu->fd,stu->uid[0],stu->uid[1],stu->uid[2],stu->uid[3],stu->uid[4]);
		#endif
		return stu;
	}
	else
	{
		#ifdef HASH_LOG
		printf("not found\n");
		#endif
		return NULL;
	}
	

}


/**
return 
2:key has exits  
1: add new key sucess*/
unsigned char rc_set_putianInfo_by_key(int *key,unsigned char key_size,st_putianInfo *value)
{
	unsigned char ret=0;

	st_putianInfo *stu =(st_putianInfo *)hash_lookup_entry(putian_hash,key,key_size);
	
	if(stu)
	{
		#ifdef HASH_LOG
		printf("update putianInfo_by_key exits");
		#endif
		stu->uid[0]=value->uid[0];
		stu->uid[1]=value->uid[1];
		stu->uid[2]=value->uid[2];
		stu->uid[3]=value->uid[3];
		stu->uid[4]=value->uid[4];
		stu->uid[5]=value->uid[5];
		
	}
	else
	{
		ret = hash_add_entry(putian_hash, key,key_size,value,sizeof(st_putianInfo));
	}
	
	


	return ret;

}


void rc_delete_putianInfo_by_key(int *key,unsigned char key_size)
{
	
	hash_free_entry(putian_hash,key,key_size);


}

