#ifndef __MQTT_CONTROL_H
#define __MQTT_CONTROL_H

#include "putian_protocol.h"
#include "Cbuffer.h"
#include "mqtt.h"
#include "cJSON.h"

typedef enum{
  Topic_Pub_PUSH = 0,
  Topic_Pub_PTlamp_PUSH,
  Topic_Pub_PTlamp2_PUSH,
  Topic_Pub_FEEDBACK
}Message_Publish_Topic;


typedef enum mqtt_state_type{
  Mqtt_State_Idle    = 0,
  Mqtt_State_tcp_connect,	
  Mqtt_State_connect_req,
  Mqtt_State_connect_req_wait,
  Mqtt_State_sub_req,
  Mqtt_State_sub_req_wait,
  Mqtt_State_connect_ok,
  Mqtt_State_Retain_Bug_wait
  
}Mqtt_State_Type;







typedef struct
{
	unsigned   char 		tcp_rev_buf[1024];
	unsigned   short		write_index;
	unsigned   short		read_index;
	
} TcpRevBuff;


extern TcpRevBuff tcp_revbuf;


#define TCP_RECV_DATA_MAX 901
typedef struct
{
	unsigned char 	data[TCP_RECV_DATA_MAX];
	
	unsigned char 	name[36];
	unsigned char 	snid[36];
	unsigned char 	msgID[36];
	unsigned char 	proj_id[36];
	char  temporary_arry[36];
	int temporary;
	unsigned char  result_ack;
}TcpReceived_Data;


extern void mqtt_ping_req(void);
extern void MQTT_Connect_Check_Per2S(void);
extern void mqtt_publish_req(unsigned char *snd_buf,Message_Publish_Topic topic);


void rc_use_mqtt_init(void);

unsigned char MQTT_start_task(void);



#endif

