#include "Cbuffer.h"





/* 初始化环形缓冲区 */
int        cbuf_init(cbuf_t *c)
{
    int    ret = OPER_OK;

    if((ret = mutex_init(&c->mutex)) != OPER_OK)    
    {

        return ret;
    }

    if((ret = cond_init(&c->not_full)) != OPER_OK)    
    {

        mutex_destroy(&c->mutex);
        return ret;
    }

    if((ret = cond_init(&c->not_empty)) != OPER_OK)
    {

        cond_destroy(&c->not_full);
        mutex_destroy(&c->mutex);
        return ret;
    }

    c->next_in    = 0;
    c->next_out = 0;
	c->size     = 0;
    return ret;
}

/* 销毁环形缓冲区 */
void        cbuf_destroy(cbuf_t    *c)
{
    cond_destroy(&c->not_empty);
    cond_destroy(&c->not_full);
    mutex_destroy(&c->mutex);


}


/* 压入数据 */
int        cbuf_enqueue(cbuf_t *c,void *data)
{
    int    ret = OPER_OK;

    if((ret = mutex_lock(&c->mutex)) != OPER_OK)    return ret;

    /*
     * Wait while the buffer is full.
     */
    while(cbuf_full(c))
    {

        cond_wait(&c->not_full,&c->mutex);
    }

    c->data[c->next_in++] = data;
    c->size++;
    c->next_in %= CAPACITY;

    mutex_unlock(&c->mutex);

    /*
     * Let a waiting consumer know there is data.
     */
    cond_signal(&c->not_empty);


    return ret;
}



/* 取出数据 */
void*        cbuf_dequeue(cbuf_t *c)
{
    void     *data     = NULL;
    int    ret     = OPER_OK;

    if((ret = mutex_lock(&c->mutex)) != OPER_OK)    return NULL;

       /*
     * Wait while there is nothing in the buffer
     */
    while(cbuf_empty(c))
    {

        cond_wait(&c->not_empty,&c->mutex);
    }

    data = c->data[c->next_out++];
	c->size--;
    c->next_out %= CAPACITY;

    mutex_unlock(&c->mutex);


    /*
     * Let a waiting producer know there is room.
     * 取出了一个元素，又有空间来保存接下来需要存储的元素
     */
    cond_signal(&c->not_full);


    return data;
}


/* 判断缓冲区是否为满 */
int        cbuf_full(cbuf_t    *c)
{
    return (c->size == CAPACITY);
}

/* 判断缓冲区是否为空 */
int        cbuf_empty(cbuf_t *c)
{
    return (c->size == 0);
}






/*
xuzhihui modifyied



*/


cbuf_t tcp_send_cmd={0};

int   rc_tcp_send_data_enqueue(cbuf_t *c,struct RC_TCP_SEND_DATA *data)
{
	struct RC_TCP_SEND_DATA* tmp;
	tmp=(struct RC_TCP_SEND_DATA*)malloc(sizeof(struct RC_TCP_SEND_DATA));
	memcpy(tmp,data,sizeof(struct RC_TCP_SEND_DATA));
	

	return cbuf_enqueue(c,tmp);
}

struct RC_TCP_SEND_DATA* rc_tcp_send_data_dequeue(cbuf_t *c)
{
	return (struct RC_TCP_SEND_DATA*)cbuf_dequeue(c);
}



cbuf_t tcp_recv_cmd={0};

int   rc_tcp_recv_data_enqueue(cbuf_t *c,struct RC_TCP_RECV_DATA *data)
{
	struct RC_TCP_RECV_DATA* tmp;
	tmp=(struct RC_TCP_RECV_DATA*)malloc(sizeof(struct RC_TCP_RECV_DATA));
	memcpy(tmp,data,sizeof(struct RC_TCP_RECV_DATA));
	

	return cbuf_enqueue(c,tmp);
}

struct RC_TCP_RECV_DATA* rc_tcp_recv_data_dequeue(cbuf_t *c)
{
	return (struct RC_TCP_RECV_DATA*)cbuf_dequeue(c);
}








cbuf_t mqtt_send_cmd={0};

int   rc_mqtt_send_data_enqueue(cbuf_t *c,struct RC_MQTT_SEND_DATA *data)
{
	struct RC_MQTT_SEND_DATA* tmp;
	tmp=(struct RC_MQTT_SEND_DATA*)malloc(sizeof(struct RC_MQTT_SEND_DATA));
	memcpy(tmp,data,sizeof(struct RC_MQTT_SEND_DATA));
	

	return cbuf_enqueue(c,tmp);
}

struct RC_MQTT_SEND_DATA* rc_mqtt_send_data_dequeue(cbuf_t *c)
{
	return (struct RC_MQTT_SEND_DATA*)cbuf_dequeue(c);
}


