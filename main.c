#include "main.h"



using namespace std;


void *timer_task(void *data)
{
	unsigned char t=0;
	
	while (1)
	{
		if(t==15)
		{	
			mqtt_ping_req();
			t=0;
		}

		MQTT_Connect_Check_Per2S();

		
		sleep(2);
		t++;
		
	
		
	}
	return NULL;
}



int main(int argc, const char *argv[])
{
	
	unsigned char system_status=0;
	unsigned short int timeout=0;
	
	pthread_t  epoll_server_start_tid,tcp_recv_porcess_pthread_tid,tcp_send_pthread_tid;
	pthread_t timer_tid;
	
	
	while(1)
	{
		switch(system_status)
		{
			case 0:
				rc_use_hash_init();
				rc_use_mqtt_init();
				rc_use_tcp_init();
				system_status = 1;
				break;
			case 1:
				if(MQTT_start_task()==2)
				{
					system_status =2;
					log4c(LOG_PATH,"MQTT:connect ok!\r\n");					
				}else
				{
					timeout++;
					if(timeout>100)
					{
						exit(0);
					}
				
				}
				break;
			case 2:
				pthread_create(&timer_tid,NULL,timer_task,0);
				log4c(LOG_PATH,"timer_task creat ok,%d!\r\n",1);
				system_status =3;
				break;
			case 3:
				
				pthread_create(&epoll_server_start_tid,NULL,epoll_server_start,0);
				pthread_create(&tcp_recv_porcess_pthread_tid,NULL,tcp_recv_porcess_pthread,0);
				pthread_create(&tcp_send_pthread_tid,NULL,tcp_send_pthread,0);
				system_status =4;
				break;
			case 4:
				
				sleep(2);
				break;
			default:
				sleep(1);
				break;

		}
		
		
	}
	
}

