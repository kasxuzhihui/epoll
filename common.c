#include "common.h"



void	hc_dsptrn( const uint8_t *target_ptr ,uint8_t *start_ptr,uint16_t size8)
{
	while( size8 != 0x00 ){
		*start_ptr = *target_ptr;
		target_ptr += 1;
		start_ptr += 1;
		size8 -= 1;
	}
} 

int log4c(char *filepath, char *fmt, ...)
{
	FILE * fp;
 
	va_list ap;
	va_start(ap, fmt);
 
	fp = fopen (filepath, "a+");
	int res = vfprintf(fp, fmt, ap);
 
	fclose(fp);
	va_end(ap);
 
	return res;
}